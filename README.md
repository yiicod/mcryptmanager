McryptManager base on CSecurityManager
==========

Config
------

```php
'components' => array(
    ...
    'mcryptmanager' => array(
        'class' => 'yiicod\mcryptmanager\McryptManager',
    )
    ...
)
```

Using
-----

```php
Yii::app->security->encrypt($data, key);
Yii::app->security->decrypt($data, key);
```  

Model behaviors
-----

```php
'McryptBehavior' => array(
    'class' => 'yiicod\mcryptmanager\models\behaviors\McryptBehavior',
    'encryptAttributes' => array('title', 'wish'),
    'encryptionKeyEval' => 'getEncryptionKey()',
    'encryptionEnableEval' => 'isEncrypt()',
),
```  