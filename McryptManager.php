<?php

namespace yiicod\mcryptmanager;

use CApplicationComponent;
use Yii;
use CMap;

class McryptManager extends CApplicationComponent
{

    /**
     * @var ARRAY $components 
     */
    public $components = array();

    public function init()
    {
        parent::init();
        //Merge main extension config with local extension config
        $config = include(dirname(__FILE__) . '/config/main.php');
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $this->{$key} = CMap::mergeArray($value, $this->{$key});
            } elseif (null === $this->{$key}) {
                $this->{$key} = $value;
            }
        }

        //Set components
        Yii::app()->setComponents($this->components);
    }

}
