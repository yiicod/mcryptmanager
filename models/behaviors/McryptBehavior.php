<?php

namespace yiicod\mcryptmanager\models\behaviors;

use Yii;
use CActiveRecordBehavior;
use CComponent;

class McryptBehavior extends CActiveRecordBehavior
{

    /**
     * @var ARRAY $encryptAttributes Attributtes which must be encrypt
     */
    public $encryptAttributes = array();

    /**
     * @var string the name of the hashing algorithm to be used by {@link computeHMAC}.
     * See {@link http://php.net/manual/en/function.hash-algos.php hash-algos} for the list of possible
     * hash algorithms. Note that if you are using PHP 5.1.1 or below, you can only use 'sha1' or 'md5'.
     *
     * Defaults to 'sha1', meaning using SHA1 hash algorithm.
     * @since 1.1.3
     */
    public $hashAlgorithm = 'sha1';

    /**
     * @var mixed the name of the crypt algorithm to be used by {@link encrypt} and {@link decrypt}.
     * This will be passed as the first parameter to {@link http://php.net/manual/en/function.mcrypt-module-open.php mcrypt_module_open}.
     *
     * This property can also be configured as an array. In this case, the array elements will be passed in order
     * as parameters to mcrypt_module_open. For example, <code>array('rijndael-256', '', 'ofb', '')</code>.
     *
     * Defaults to 'des', meaning using DES crypt algorithm.
     * @since 1.1.3
     */
    public $cryptAlgorithm = 'rijndael-256';

    /**
     * @var string $encryptionKey Encryption key
     */
    public $encryptionKey = null;

    /**
     * @var string Encrypt/Decrypt key generatore 
     */
    public $encryptionKeyEval = null;

    /**
     * @var boolean $encryptionEnable Enable encrypt for model
     */
    public $encryptionEnable = null;

    /**
     * @var boolean $encryptionEnable Enable encrypt for model
     */
    public $encryptionEnableEval = null;

    /**
     * Prefix for define encrypt value
     * @retrun string 
     */
    protected function getPrefix()
    {
        return md5('SecurityBehavior');
    }

    protected function getEncryptionKey()
    {
        if (!empty($this->encryptionKey)) {
            return $this->encryptionKey;
        } elseif (!empty($this->encryptionKeyEval)) {
            $this->encryptionKey = eval('return ' . $this->encryptionKeyEval . ';');
            return $this->encryptionKey;
        }
        throw new Exception('Empty encrypt key');
    }

    protected function getEncryptionEnable()
    {
        if (!empty($this->encryptionEnable)) {
            return $this->encryptionEnable;
        } elseif (!empty($this->encryptionEnableEval)) {
            $this->encryptionEnable = eval('return ' . $this->encryptionEnableEval . ';');
            return $this->encryptionEnable;
        }
        throw new Exception('Empty encript enable');
    }

    /**
     * Check value if this encrypt
     * @retrun boolean
     */
    public function isEncrypt($value)
    {
        return !(strpos($value, $this->getPrefix()) === false);
    }

    protected function encryptAttributes()
    {
        if ($this->getEncryptionEnable()) {
            foreach ($this->encryptAttributes as $attribute) {
                if ($this->getOwner()->{$attribute}) {
                    $this->getOwner()->{$attribute} = Yii::app()->security->encrypt($this->getOwner()->{$attribute}, $this->getEncryptionKey());
                    $this->getOwner()->{$attribute} = $this->getOwner()->{$attribute} . $this->getPrefix();
                }
            }
        }
    }

    protected function decryptAttributtes()
    {
        foreach ($this->getOwner()->attributeNames() as $attribute) {
            if ($this->isEncrypt($this->getOwner()->{$attribute})) {
                $this->getOwner()->{$attribute} = str_replace($this->getPrefix(), '', $this->getOwner()->{$attribute});
                $this->getOwner()->{$attribute} = Yii::app()->security->decrypt($this->getOwner()->{$attribute}, $this->getEncryptionKey());
            }
        }
    }

    /**
     * After find event
     * Decrypt if need
     */
    public function afterFind($event)
    {
        parent::afterFind($event);
        $this->decryptAttributtes();
    }

    /**
     * Before save event
     * Encrypt if need
     */
    public function beforeSave($event)
    {
        parent::beforeSave($event);
        $this->encryptAttributes();
    }

    /**
     * Before save event
     * Encrypt if need
     */
    public function afterSave($event)
    {
        parent::beforeSave($event);
        $this->decryptAttributtes();
    }

}
